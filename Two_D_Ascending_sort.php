<?php 

$arr = [  
    [56, 95, 88, 73, 29],
    [72, 27, 10, 54, 20],
    [91, 98, 38, 69, 28],
    [97, 15, 39, 57, 60],
    [10, 16, 06, 05, 50],
    [65, 99, 15, 96, 55],
    [35, 50, 46, 92, 61],
    [30, 29, 50, 100, 81]
];

echo "<pre>";
print_r(ascending_2d($arr));
echo "<pre>";

/**
 * This function will take 2 dimenional numbers array and return the 2D array wiht assending sorted array at each index
 * @return array
 */
function ascending_2d($matrix) {

    $arr = $matrix; 
    // Taking the size of 2D array into a variable
    $size = sizeof($arr);

    // Outer loops for fetching all the arrays from 2D array
    for ( $k = 0; $k < $size; $k++) {

        // Inner loops doing there job just ass ascending sort also os doing using Selection Sort algo
        for ($i=0; $i < sizeof($arr[$k]) ; $i++) {
            for ($j=$i+1; $j < sizeof($arr[$k]) ; $j++) {
    
                // Condition check for swaping element
                if($arr[$k][$j] < $arr[$k][$i])
                {
                    // Swaping elements
                    $temp = $arr[$k][$i];
                    $arr[$k][$i] = $arr[$k][$j];
                    $arr[$k][$j] = $temp;
                    
                }
    
            }
        }
    }

    return $arr;
}