<?php 

$array = [4, 6, 8, 8];

echo "<pre>";
print_r(sort_ascending($array));
echo "<pre>";

/**
 * This function will take an numbers array and return the array assending sorted
 * @return array
 */
function sort_ascending($array) {
    $arr = $array ;
    // Taking array size into variable
    $size = sizeof($arr);

    // Loop through each element of the array and sorting using Selection Sort algo
    // (The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.)
    for ($i=0; $i < $size ; $i++) {

        // 
        for ($j=$i+1; $j < $size ; $j++) {

            // Condition check for swaping element
            if($arr[$j] < $arr[$i])
            {
                // Swaping elements
                $temp = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $temp;
            }

        }
    }
    return $arr;
}