<?php 

$array = [4, 90, 3, 9];
echo "<pre>";
print_r(sort_descending($array));
echo "<pre>";

/**
 * This function will take an numbers array and return the array decending sorted
 * @return array
 */
function sort_descending($array) {
    $arr = $array ;
    // Taking array size into variable
    $size = sizeof($arr);

    // Loop through each element of the array and sorting using Selection Sort algo
    // (The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.)
    // The only change is the condition that is reversed in decending case.
    for ($i=0; $i <$size ; $i++) { 
        for ($j=$i+1; $j <$size ; $j++) { 
            // Condition check for swaping element
            // Condition Reversed
            if($arr[$i] < $arr[$j])
            {
                // Swaping elements
                $temp = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $temp;
            }        
        }
    }    
    return $arr;
}